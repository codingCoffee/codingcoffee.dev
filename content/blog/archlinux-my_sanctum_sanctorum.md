---
title: "Archlinux - My Sanctum Sanctorum"
publishdate: 2018-02-13T13:56:12-05:00
tags: ["archlinux","linux","systemd","pacman"]
type: "post"
draft: false
showDate: true
comments: false
---

This guide is the only guide you'll ever need (lol) to set up your **Archlinux** system from ground up. Follow this only when you have gained familiarity to the **Linux** ecosystem using **Ubuntu** or any other OS. Preferable don't skip directly from **Windows** to **Arch**. You might wanna give **Manjaro** a try if you're coming from Windows, and are really keen on doing things the **Arch** way, since it's based on **Arch**, however has a GUI to help you out with most of the stuff. Well if you have time to kill and the internet at your disposal then you may begin your journey into the wonderful and awesome world of **Arch Linux**!

## Requirements:

- Internet
- An old/new PC or laptop (I use Arch as my daily driver)
- Patience
- Patience
- Patience

Make sure you have at least **a few hours** of spare time.

## Create a bootable USB for Archlinux

On a Linux machine, connect the USB, and figure out the name of your USB
using

```bash
lsblk
```

Run the following command, replacing **{x}** with your drive, e.g. **/dev/sdb**,
if {x} = b. (**Do not append a partition number**, so do not use something
like **/dev/sdb1**)

```bash
dd bs=4M if={path_to_archlinux.iso} of=/dev/sd{x} status=progress oflag=sync
```

## Installation

- Boot into BIOS and **disable Secure Boot**. **UEFI is ON** but Secure Boot
  is disabled. If you're not able to access the Secure Boot settings,
  set a Supervisor Password in BIOS and then try again.
- If you have a wired internet connection, plug-in the ethernet cable
  before booting the PC.
- Verify the Boot mode and ensure its UEFI. Do this by executing the
  command below and verifying the directory exists

  ```bash
  ls /sys/firmware/efi/efivars
  ```

- Check the internet connection. Remember internet connection is
  mandatory.

  ```bash
  ping -c3 google.com
  ```

- If connected, skip to the next step. Check the output of `ip link`
  command. It should show a list of network interfaces to connect to.
  The interfaces starting with 'w' are usually the ones dealing with
  WiFi, and those starting with 'e' deal with the ethernet
  connection.

  ```bash
  ip link
  ```

  If you have a wired ethernet cable, follow these steps, else, skip
  to the WiFi section. Connect your LAN cable and execute this

  ```bash
  systemctl start dhcpcd@{ethernet_interface_name}.service
  ```

  Try pinging again. You should be connected to the internet now.

#### Connection to WiFi:

  - Find/Confirm your WiFi interface

  ```bash
  iw dev
  ```

  - Activate it using the {wifi~interfacename~}

    ```bash
    ip link set {wifi_interface_name} up
    ```

  - Check if there's 'UP' in between '\<' and '\>'.
    {wifi~interfacename~} will be referred to as {interface}
    henceforth

    ```bash
    ip link
    ```

  - Scan for WiFi

    ```bash
    iw dev {interface} scan | less
    ```

    If you're using it in adhoc mode (a mode in which there is no
    central controller), you'll have to mention it, using this
    command. If you're trying to connect to a WiFi router, it's
    NOT adhoc mode, and you DON'T need to execute this command,
    since the WiFi router acts as a central controller.

    ```bash
    ip link set {interface} down
    iw dev {interface} set type ibss
    ip link set {interface} up
    ```

    -   Make a note of your
        -   SSID (name of your network)
        -   Security
    -   Encryption
        -   If there is no encryption on your WiFi, use

            ```bash
            iw dev {interface} connect "{your_ESSID}"
            ```

        -   If there is encryption of type WEP
            -   using a hexadecimal or ASCII key (the format is
                distinguished automatically, because a WEP key has a
                fixed length):

                ```bash
                iw dev {interface} connect "{your_ESSID}" key 0:{your_key}
                ```

            -   using a hexadecimal or ASCII key, specifying the
                third set up key as default (keys are counted from
                zero, four are possible):

                ```bash
                iw dev interface connect "{your_ESSID}" key d:2:{your_key}
                ```

        -   If it's WPA or WPA2
            -   A passphrase is generated using

                ```bash
                wpa_passphrase {your_SSID} {passphrase}
                ```

            -   Connect to the network using

                ```bash
                wpa_supplicant -B -i {interface} -c <(wpa_passphrase {MYSSID} {passphrase})
                ```

            -   Get an IP address allocated to your PC using dhcp

                ```bash
                dhcpcd {interface}
                ```

  - Check if the interface is connected to the WiFi using

    ```bash
    iw dev {interface} link
    ```

  - Check the amount of bytes transmitted and received using

    ```bash
    iw dev {interface} station dump
    ```

  For more details visit [this page](https://wiki.archlinux.org/index.php/Wireless_network_configuration)

- You can also SSH into this system if you want to execute commands
  remotely for ease of use:

  ```bash
  systemctl start sshd
  ```

  Now find the IP of this system using

  ```bash
  ip addr
  ```

  And to connect to this system remotely, execute this on the PC you
  want to connect to it form

  ```bash
  ssh {ip_addredd} -l root
  ```

- Set date

  ```bash
  timedatectl set-ntp true
  ```

- Partitioning the disk
  -   Check the contents of the disk

      ```bash
      lsblk
      ```

  -   Make sure none of the partitions you want to make are mounted by
      checking the MOUNTPOINT using

      ```bash
      lsblk -f
      ```

      In case they are mounted, use

      ```bash
      umount /dev/sd{device_letter}{partition_number}
      ```

      Do this for all the mounted partitions
  -   Making partitions

      ```bash
      gdisk /dev/sd{device_letter}
      ```

      -   Delete everything and create a new partition table

          ```bash
          o
          ```

      -   Create a new EFI partition

          ```bash
          n
          ```

          -   Partition number (1-128, default 1):

              ```bash
              # press RETURN for default
              ```

          -   First Sector (34-976773134, default=2048) or
              {+-}size{KMGPT}:

              ```bash
              # press RETURN for default
              ```

          -   Last Sector (2048-976773134, default=976773134) or
              {+-}size{KMGPT}:

              ```bash
              +512M
              ```

          -   Current type is 'Linux filesystem' HEX code or GUID (L
              to show codes, Enter = 8300):

              ```bash
              EF00
              ```

              This makes it an EFI partition

      -   Create a new SWAP partition

          ```bash
          n
          ```

          -   Partition number (2-128, default 2):

              ```bash
              # press RETURN for default
              ```

          -   First Sector (34-976773134, default=1050624) or
              {+-}size{KMGPT}:

              ```bash
              # press RETURN for default
              ```

          -   Last Sector (1050624-976773134, default=976773134) or
              {+-}size{KMGPT}:

              ```bash
              +20G
              ```

          -   Current type is 'Linux filesystem' HEX code or GUID (L
              to show codes, Enter = 8300):

              ```bash
              8200
              ```

              This makes it a SWAP partition

      -   Create a new root partition

          ```bash
          n
          ```

          -   Partition number (3-128, default 3):

              ```bash
              # press RETURN for default
              ```

          -   First Sector (34-976773134, default=42993664) or
              {+-}size{KMGPT}:

              ```bash
              # press RETURN for default
              ```

          -   Last Sector (42993664-976773134, default=976773134) or
              {+-}size{KMGPT}:

              ```bash
              +80G
              ```

          -   Current type is 'Linux filesystem' HEX code or GUID (L
              to show codes, Enter = 8300):

              ```bash
              8304
              ```

              This makes it a root partition

      -   Create a new home partition

          ```bash
          n
          ```

          -   Partition number (3-128, default 3):

              ```bash
              # press RETURN for default
              ```

          -   First Sector (34-976773134, default=210765824) or
              {+-}size{KMGPT}:

              ```bash
              # press RETURN for default
              ```

          -   Last Sector (210765824-976773134, default=976773134) or
              {+-}size{KMGPT}:

              ```bash
              # press RETURN for default, to select the remainder of the disk
              ```

          -   Current type is 'Linux filesystem' HEX code or GUID (L
              to show codes, Enter = 8300):

              ```bash
              8300
              ```

              This makes it a home partition

      Now write these changes to the disk

      ```bash
      w
      ```

      And proceed with a yes (`Y`) Now check if the partitions were
      made according to your liking

      ```bash
      lsblk
      ```

  -   Formatting partitions

      ```bash
      mkfs.fat -F32 /dev/sda1  # format EFI partition as FAT32

      mkswap /dev/sda2         # format SWAP partition as SWAP
      swapon /dev/sda2

      mkfs.ext4 /dev/sda3      # format root partition as EXT4

      mkfs.ext4 /dev/sda4      # format home partition as EXT4
      ```

- Mounting the file system. Same procedure to be followed for
  debugging Archlinux

  ```bash
  mount /dev/sda3 /mnt      # mount root

  mkdir -p /mnt/boot        # create boot directory in mount
  mount /dev/sda1 /mnt/boot # mount boot directory

  mkdir -p /mnt/home        # create boot directory in mount
  mount /dev/sda4 /mnt/home # mount boot directory
  ```

- Install Archlinux

  ```bash
  pacstrap /mnt base base-devel
  ```

- Generate an fstab file. The fstab file can be used to define how
  disk partitions, various other block devices, or remote filesystems
  should be mounted into the filesystem.

  ```bash
  genfstab -U /mnt >> /mnt/etc/fstab
  ```

  Check the resulting file in /mnt/etc/fstab afterwards, and edit it
  in case of errors.

- Change root into the new system. Chroot is an operation that changes
  the apparent root directory for the current running process and
  their children. Changing root is commonly done for performing system
  maintenance on systems where booting and/or logging in is no longer
  possible.

  ```bash
  arch-chroot /mnt
  ```

- Set the timezone

  ```bash
  ln -sf /usr/share/zoneinfo/{Region}/{City} /etc/localtime # Asia/Kolkata
  ```

- Run hwclock to generate /etc/adjtime:

  ```bash
  hwclock --systohc
  ```

- Uncomment `en_US.UTF-8 UTF-8` and other needed localizations in
  /etc/locale.gen, and generate them with

  ```bash
  locale-gen
  ```

- Set the LANG variable in /etc/locale.conf accordingly, for example:

  ```
  LANG=en_US.UTF-8
  LC_CTYPE=en_US.UTF-8
  LC_MESSAGES=en_US.UTF-8
  LC_ALL=en_US.UTF-8
  LANGUAGE="en_US.UTF-8"
  ```

- Change the keyboard layout, by the /etc/vconsole.conf file. It is US
  English by default

  ```
  KEYMAP=us
  ```

- Create hostname by editing /etc/hostname

  ```
  {my_host_name}
  ```

- Add this to /etc/hosts, if it doesn't exist

  ```text
  127.0.0.1 localhost.localdomain localhost
  ::1   localhost.localdomain localhost
  127.0.1.1 {my_host_name}.localdomain  {my_host_name}
  ```

- Creating a new initramfs is usually not required, because mkinitcpio
  was run on installation of the linux package with pacstrap. But run
  it anyways

  ```bash
  mkinitcpio -p linux
  ```

- Set the root password using

  ```bash
  passwd
  ```

- Install intel-ucode. It provides microcode updates for Intel CPUs

  ```bash
  pacman -S intel-ucode
  ```

- To use SystemD as the bootloader follow these steps. Alternatively
  you may use GRUB, skip this section to use GRUB
  -   Installing the bootloader systemd-boot

      ```bash
      bootctl install
      bootctl update
      ```

  -   Edit the /boot/loader/loader.conf file and remove the long text
      and replace it with

      ```
      default arch
      ```

  -   Adding boot entries. Create a file
      `/boot/loader/entries/arch.conf`

      ```bash
      blkid -s UUID -o value /dev/sd{device_letter}{root_partition_number} > /boot/loader/entries/arch.conf
      ```

      The UUID of the root partition will be extracted and added to
      the arch.conf file.

  -   Now edit the arch.conf file and add the following.

      ```
      title  Arch Linux
      linux  /vmlinuz-linux # it is vmlinuz not vmlinux
      initrd  /intel-ucode.img # intel ucode thing provided you have installed it already
      initrd  /initramfs-linux.img
      options root=UUID={UUID_of_root_partition} rw #  rw at the end is important
      ```

  -   For hibernation, increase the size of sys/power/image~size~, if
      you have a large SWAP partition, and decrease if you have a
      small one. By default it is 2/5 th the RAM size. Then make an
      entry in arch.conf

      ```bash
      blkid -s UUID -o value /dev/sd{device_letter}{swap_partition_number} >> /boot/loader/entries/arch.conf
      ```

      This'll put the UUID of the SWAP partition at the end of the
      arch.conf file. Now modify the last line of the conf file

      ```
      options root=UUID={UUID_of_root_partition} resume=UUID={UUID_of_swap_partition} rw
      ```

- Install packages to ease installation after boot List of packages
  needed
  -   iproute2: for `ip` to manage networking
  -   acpi: for checking battery percentage and stuff
  -   iw: for wireless connections
  -   wpa~supplicant~: for wireless WPA protected connections
  -   git: a version control system
  -   dialog: A tool to display dialog boxes from shell scripts
  -   vim: a text editor
  -   zsh: shell
  -   curl: fetch things from internet
  -   wget: fetch things from internet
  -   aria2: the best download manager
  -   xdg-user-dirs: auto create and link \$HOME directory folders
      like Documents, Downloads etc

  ```bash
  pacman -S iproute2 acpi iw wpa_supplicant dialog vim zsh git curl wget aria2 stow networkmanager xdg-user-dirs
  ```

- Reboot PC after unmounting the mounted partition

  ```bash
  exit
  umount -R /mnt
  poweroff
  ```

- Remove the installation media and start PC, and connect to internet

- Optionally add your keys and turn on SSH for remote access

  ```bash
  mkdir -p ~/.ssh
  curl -L https://github.com/{username}.keys >> ~/.ssh/authorized_keys
  systemctl restart sshd
  ```

  You may SSH into the system now

- Check battery

  ```bash
  acpi -b
  ```

- Activating zsh and using antigen

  ```bash
  chsh -s $(which zsh)
  curl -L git.io/antigen > $HOME/antigen.zsh
  curl -L 'https://raw.githubusercontent.com/codingCoffee/effective-enigma/master/.zshrc' > $HOME/.zshrc
  exec zsh
  ```

- Editing the /etc/pacman.conf for the better.
  1.  Uncomment and/or add this.

      ```
      #UseSyslog
      #UseDelta #just mentioning, not fully supported in arch
      Color
      TotalDownload
      CheckSpace
      VerbosePkgLists
      ILoveCandy
      ```

  2.  Skipping this step won't cause any harm Use `aria2c` as the
      download manager instead of wget. Add the following new entry
      just below the \"\#XferCommand = /usr/bin/curl -C - -f %u \>
      %o\" line. The 2 caveats are you won't be able to see the
      overall download size, but downloads will be faster, and
      everytime you do `pacman -Syu` the database files will be
      downloaded again.

      ```
      XferCommand = /usr/bin/aria2c --allow-overwrite=true --continue=true --file-allocation=none --log-level=error --max-tries=2 --max-connection-per-server=5 --max-file-not-found=1 --min-split-size=1M --no-conf --remote-time=true --summary-interval=0 --timeout=3 --dir=/ --out %o %u
      ```

- Create a user

  ```bash
  useradd -m -G wheel {your_username}
  ```

  Assign password to user

  ```bash
  passwd {your_username}
  ```

  Give the user sudo access by un-commenting `%wheel ALL=(ALL) ALL`

  ```bash
  visudo
  ```

  su into your user

  ```bash
  su - {your_username}
  ```

- Create the default folders using the xdg-user-dirs-update service

  ```bash
  xdg-user-dirs-update
  ```

- Now installing the main stuff
  -   plasma: KDE desktop environment and other things Select when
      asked
      -   ibglvnd over nvidia-304xx-utils
      -   phonon-qt5-gstreamer over phonon-qt5-vlc
      -   libx264 over libx264-10bit
  -   docker
  -   firefox
  -   aria2c
  -   libreoffice-fresh
  -   dolphin
  -   dolphin-plugins
  -   zip
  -   unzip
  -   unrar
  -   vlc
  -   p7zip
  -   rsync
  -   syncthing
  -   konsole
  -   openssh
  -   git
  -   blender
  -   krita
  -   gimp
  -   inkscape
  -   scribus
  -   kwallet
  -   kwalletmanager
  -   networkmanager
  -   gst-plugins-bad: gstreamer plugins
  -   gst-plugins-ugly: gstreamer plugins
  -   gst-plugins-good: gstreamer plugins
  -   digikam: (gwenview alternative)
  -   spectacle: window management
  -   kio: Resource and network access abstraction
  -   kio-extras: extra utilities for kio
  -   ntfs-3g: NTFS filesystem driver and utilities
  -   fcron: feature rich cron implementation
  -   cups: printing daemon
  -   packagekit-qt5: installs packagekit as well (PackageKit is a
      system designed to make installing and updating software on your
      computer easier. The primary design goal is to unify all the
      software graphical tools used in different distributions, and
      use some of the latest technology like PolicyKit)
  -   print-manager
  -   gtk3: (for gtk3-print-backends)
  -   imagemagick: powerful tool to deal with images
  -   graphicsmagick: (imagemagick - bloat), however not as powerful
      or feature rich as imagemagick, but faster in some cases
  -   mesa: an open-source implementation of the OpenGL specification
  -   clementine: music player
  -   okular: pdf reader
  -   latte-dock
  -   gparted
  -   screenfetch: to generate a cool ASCII Art based on linux distro
      you're using
  -   bluez: bluetooth protocol stack
  -   bluez-utils: bluetoothctl utility
  -   pulseaudio-bluetooth: for interfacing bluetooth earphones
  -   thefuck: auto fix your commands
  -   linux-headers
  -   whois: reverse lookup IPs to domains
  -   noto-fonts: for normal fonts
  -   noto-fonts-cjk: for chinese, japanese and korean fonts
  -   noto-fonts-emoji: for emoji support
  -   noto-fonts-extra: for fonts
  -   arp-scan: commandline utility for scanning devices on network
  -   gtkglext: for any desk

  ```bash
  pacman -S plasma docker firefox aria2c libreoffice-fresh dolphin dolphin-plugins zip unzip unrar vlc p7zip rsync syncthing konsole openssh xdg-user-dirs git blender krita gimp inkscape scribus kwallet kwalletmanager networkmanager gst-plugins-bad gst-plugins-ugly gst-plugins-good digikam spectacle kio kio-extras ntfs-3g fcron cups packagekit-qt5 print-manager gtk3 imagemagick graphicsmagick mesa clementine okular latte-dock gparted screenfetch bluez bluez-utils pulseaudio-bluetooth thefuck linux-headers whois gtkglext
  ```

- Switch back to root using

  ```bash
  exit
  ```

- Final Steps to power up the System in GUI mode

      systemctl enable NetworkManager    # for Network discovery
      systemctl enable org.cups.cupsd    # for printing services
      systemctl enable sddm              # for GUI
      systemctl enable bluetooth         # for bluetooth
      systemctl enable sshd              # for ssh server

  Now create the sddm config

  ```bash
  sddm --example-config > /etc/sddm.conf
  ```

  Now edit the /etc/sddm.conf file to modify the two variables
  (Current, and CursorTheme) as shown

  ```
  [Theme]
  Current=breeze
  CursorTheme=breeze_cursors
  ```

- Optionally comment the root entry from /etc/fstab if you're using
  systemd-boot as the boot manager instead of grub. (It'll make the
  bootup a little faster :P)

- Reboot your system

  ```bash
  reboot
  ```

- Install Sublime Text from its official repo Install the GPG key:

  ```bash
  curl -O https://download.sublimetext.com/sublimehq-pub.gpg && sudo pacman-key --add sublimehq-pub.gpg && sudo pacman-key --lsign-key 8A8F901A && rm sublimehq-pub.gpg
  ```

  Select the channel to use:

  ```bash
  echo -e "\n[sublime-text]\nServer = https://download.sublimetext.com/arch/stable/x86_64" | sudo tee -a /etc/pacman.conf
  ```

  Update pacman and install Sublime Text

  ```bash
  sudo pacman -Syu sublime-text
  ```

- Building packages from AUR post booting into GUI mode

  ```bash
  git clone {git_rpeo_url}
  makepkg -sci
  ```

  -s: sync dependencies and install them if any -i: install the
  package after building it

- Packages to install from AUR
  -   google-chrome-beta
  -   spotify
- Change terminal profile
  -   Open Konsole
  -   Settings -\> Edit Current Profile -\> Select 'Linux Colors',
      then click on 'New'
  -   Modify the 1st and the 3rd background color to set 'Val' = 44
  -   Set Transparency to 10% and save


### Common Commands

1.  To just update the database

    ```bash
    pacman -Syy
    ```

2.  To update everything

    ```bash
    pacman -Syu
    ```

3.  To install a package

    ```bash
    pacman -S {package_name}
    ```

4.  To delete a package

    ```bash
    pacman -Rs {package_name}
    ```

5.  To just search the database for a package

    ```bash
    pacman -Ss {package_name}
    ```


```
DANGER!!!
Mere mortals, this is where you stop
DO NOT PROCEED FURTHER IF YOU DON'T KNOW WHAT YOU'RE DOING. IT CAN MESS UP YOUR SYSTEM REAL BAD

Learn a little about the system and then maybe come back here after a year to lean more.

YOU HAVE AN AMAZING SYSTEM UP AND RUNNING !!!
```

```
YOU HAVE BEEN WARNED!
```


-   NVIDIA Drivers for GTX 1060

    1.  Installing nvidia optimus so that both GPUs can be used. Intel
        GPU is used for normal display, and Nvidia GPU can be used at
        will for machine learning and other stuff

        ```bash
        pacman -S bumblebee mesa nvidia xf86-video-intel
        ```

    2.  Add your user to the bumblebee group

        ```bash
        gpasswd -a {your_user_name} bumblebee
        ```

    3.  Enable bumblebee

        ```bash
        systemctl enable bumblebeed
        ```

    4.  Reboot

        ```bash
        reboot
        ```

    5.  Install other necessary stuff

        ```bash
        pacman -S mesa-demos nvidia-settings
        ```

    6.  If your other GPU is intel you can use `intel_gpu_top` to check
        it's performance

        ```bash
        pacman -S intel_gpu_tools
        ```

    7.  To check the usage of your GPU

        ```bash
        nvidia-smi
        ```

    8.  In order to run anything on your GPU

        ```bash
        optirun -b none {application_name} -c :8
        ```

    9.  To install cuda and cudnn for machine learning

        ```bash
        pacman -S cuda cudnn
        ```

    10. To ensure power saving by turning off the nvidia card when it is
        not in use

        ```bash
        pacman -S bbswitch
        ```

    11. Reboot

        ```bash
        reboot
        ```

-   Install VirtualBox

    ```bash
    pacman -S virtualbox
    ```

    Use virtualbox-host-modules-arch over virtualbox-host-dkms To use
    virtualbox:

    ```bash
    modprobe vboxdrv
    ```

-   In order to reduce the shutdown time. Create a folder for your conf
    file

        mkdir /etc/systemd/system.conf.d/

    Create a file named `timeout.conf` in the `system.conf.d` folder and
    add the following to it

    ```
    [Manager]
    DefaultTimeoutStartSec=15s
    DefaultTimeoutStopSec=5s
    ```

-   To get amazing customizable touchpad gestures
    1.  Become a member of the input group

        ```bash
        gpasswd -a $USER input
        ```

        You'll have to logout and login for the gestures to workspace
    2.  Download and install from the AUR repo

            git clone https://aur.archlinux.org/libinput-gestures.git
            cd libinput-gestures
            makepkg -sci

    3.  To activate gestures at boot

        ```bash
        libinput-gestures-setup autostart
        ```

    4.  Customize gestures The guide, and the main gestures are present
        at `/etc/libinput-gestures.conf`. You can override these by
        adding a file on `$HOME/.config/libinput-gestures.conf`

        ```
        # KDE: Present Windows (Current Desktop)
        gesture swipe down 3 xdotool key ctrl+F9

        # KDE: Present Windows (All Desktops)
        gesture swipe down 4 xdotool key ctrl+F10

        # KDE: Present Windows (All Desktops)
        gesture swipe up xdotool key ctrl+F7

        # KDE: Show desktop
        gesture pinch out xdotool key ctrl+F12

        # KDE: Switch one desktop to the left
        gesture swipe right  xdotool key Super+Left

        # KDE: Switch one desktop to the right
        gesture swipe left xdotool key Super+Right
        ```

        You'll have to logout and login for changes to take effect

-   To install Redshift

    ```bash
    pacman -S python-gobject python-xdg librsvg redshift
    ```

    Create a redshift config file in \~/.config/redshift.conf

    ```
    TODO
    ```

-   \[Don't use, causes bluetooth problems\] To install power
    management

    ```bash
    pacman -S tlp
    ```

    TO activate it run these commands

    ```bash
    systemctl enable tlp.service
    systemctl enable tlp-sleep.service
    systemctl mask systemd-rfkill.service
    systemctl mask systemd-rfkill.socket
    ```

    Add the following to your \~/.profile file, where \"01:00.0\" is the
    address of your GPU. You can get this using lspci

    ```bash
    export RUNTIME_PM_BLACKLIST="01:00.0"
    ```

-   Set all fonts to noto Goto Settings -\> Fonts -\> Except for \"Fixed
    Width\" which should be \"Noto Mono\", everything else should be
    \"Noto Sana UI\" Also in Chrome do the same settings

