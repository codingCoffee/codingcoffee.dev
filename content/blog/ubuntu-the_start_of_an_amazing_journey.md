---
title: "Ubuntu - The start of an amazing journey"
publishdate: 2018-02-09T12:32:12-05:00
tags: ["linux","ubuntu","primer"]
type: "post"
draft: false
showDate: true
comments: false
---

I originally started off with the Windows Operating System. It was good until I realized I couldn't customize anything the way I wanted.

As of now I have transitioned to using Arch Linux as my daily driver since it brings along a ton of customization capabilities and a minimalistic approach with I very much like. But I usually suggest people to start off with something like Ubuntu because of the ease with which you can setup your system.

This blog post lists the problems I faced while transitioning from Windows to Ubuntu. Treat it like an FAQ of sorts, since most of the solutions can be applied to any linux distribution in general.

# UBUNTU Setup

## Windows settings

### Allocate Space for Ubuntu

If you plan to dual boot your system then you'll need to allocate space for Ubuntu. For the record, Ubuntu is as capable if not more, an OS as Windows. It just needs some time to get used to if you're coming from a Windows background.

1. Open Disk Utility
2. Select a Drive which has enough space to spare
3. Right Click and select Shrink Volume and select the number in bytes which you want to spare.

I recommend around 200 GB space, but there is no hard and fast rule, you can allocate as you like.

### Disable fastboot on Windows

Before you install Ubuntu you'll have to disable fastboot on your Windows system. Fastboot is a type of hybrid shutdown in which the data present on your RAM is stored in your hard disk for faster boot up speeds, however it renders the hard disk unusable by other operating systems, since there is cached data present on them from Windows.

1. Right click on battery symbol
2. Goto Power Options
3. On the left side select "Choose what the power buttons do"
4. Click on "Change settings that are currently unavailable"
5. Uncheck "Turn on fast startup (recommended)"

## Make a bootable USB

1. Download and install [Etcher](https://etcher.io/)
2. Download the [Ubuntu Image](https://www.ubuntu.com/download/desktop?)
3. Make a bootable USB by opening Etcher, inserting the USB and selecting the ISO file you just downloaded.

## Install Ubuntu

You're all set and ready to install Ubuntu!

1. Insert the bootable USB and restart you PC. Boot into BIOS using the function keys on startup. (On HP systems it's usually the `F10` key which needs to be pressed)
2. Highlight the USB drive and hit Return (ENTER)

The system will boot into Ubuntu and you'll have a Graphical Interface to aid you with the installation.

### Partition Scheme

The only complicated part during the installation if the partition scheme. I prefer having my `/home` and `/` (root) partitions separate and hence this the partition scheme I use.

Select "something else" from the boot window. Then partition, the 100
GB you allocated via windows, as shown below. *

```
swap  -- 11 GB - PRIMARY        (swap should roughly be 1.5x RAM but is not really mandatory)
/     -- 75 GB - LOGICAL - ext4 (recommended min 50GB)
/boot -- 1  GB - LOGICAL - ext4 (min 500MB)
/home -- 75 GB - LOGICAL - ext4 (or rest of it)
```

Feel free to resize as you see fit as per the amount of space you have to spare.

# FAQs

## Installed Ubuntu 16.04, and wanted to move to 14.04

Make a 14.04 bootable USB and boot it up. Select 'Uninstall Ubuntu
16.04 and reinstall'. \#Make sure the USB you\'re using will be able to
revert back to data storage from bootable mode, since some USB\'s don't
support this, eg. Kingston etc.

## In case the USB gets messed up and cannot be used for data storage follow this:

[Fixing your USB](http://askubuntu.com/questions/198065/how-to-format-a-usb-drive)

## Terminal
All the commands need to be executed inside a terminal. To open the terminal press `Ctrl + Alt + T`


## To update your package list and upgrade you packages
```sh
sudo apt update
sudo apt upgrade
```

*NOTE: This is when it'll ask your password for `sudo` privileges. `sudo` privileges are asked when something needs to be changed in the root directory. Think of `root` as the "C" drive on your Windows system. It has the OS installed along with all the other softwares on your system, hence you need to be a little careful while using it. Do not execute any random as command as root!*

## Install LibreOffice

### Remove LibreOffice 4.x if installed using
```sh
sudo apt-get remove \--purge libreoffice\*
sudo apt-get clean
sudo apt-get autoremove
```
### Install LibreOffice 5.x, open Terminal and type:*
```
sudo add-apt-repository ppa:libreoffice/ppa
sudo apt-get update
sudo apt-get upgrade
```

## To access an FTP server
Open file explorer, and select "Connect to server", and type in the
ftp address.

## To install Spotify

Visit [this link](https://www.spotify.com/int/download/linux/) and follow the instructions.

## Copy files using terminal

1. If you have a file in `/path/to/file` and I want to copy this file to `/new/path` and do not want to delete any thing in `/new/path` directory.

By using -i for interactive you will be asked if you would like
to replace the file:
```
cp -i /path/to/file /new/path
```
or you can use -b to create a backup of your file:
```
cp -b /path/to/file /new/path
```

2. If you want to copy a directory (folder) from `/path/to/directory` to `/path/to/new_directory/` and do not want to delete any thing on `/path/to/new_directory/`

Use -R for recursive and -i for interactive:
```
cp -Ri /path/to/directory /path/to/new_directory
```

## Moving files using terminal

If you want to cut a folder/file and copy to other place without deleting files in that directory, use the same commands as above, but use the `mv` command instead of the `cp` command

## Delete files using terminal

```
rm -rf folderName
```

Be careful with this command. If it says `Permission denied` do not simply use `sudo`. You are getting `Permission denied` because you're trying to delete something which you don't have permissions over. This could very well mean you're trying to delete something from the `root`, which is not recommended unless you know what you're doing.

Note: this is assuming you are already on the same level of the folder
you want to delete in terminal, if not:

```
rm -r /path/to/folderName
```

FYI: you can use letters -f, -r, -v:

-   -f = to ignore non-existent files, never prompt
-   -r = to remove directories and their contents recursively
-   -v = execute commands verbosely

## To install Firefox Developer 64-bit edition

### Uninstall existing Firefox installation if any
```
sudo apt-get purge firefox
```

### Download Firefox
[From here](https://www.mozilla.org/en-US/firefox/developer/all/)

After download completes extract it into the downloads folder, and rename the `firefox` folder to `firefox_dev`. Copy it to the `/opt` directory with the command
```
sudo cp -r Downloads/firefox_dev /opt
```

Open terminal and type:
```
vim ~/.local/share/applications/firefox_dev.desktop
```
I'm using `vim` here. `vim` is a text editor inside the terminal. You can choose any text editor which you like, for e.g, `nano` etc.

Then insert the following text into the file:
```
[Desktop Entry]
Name=Firefox Developer
GenericName=Firefox Developer Edition
Exec=/opt/firefox_dev/firefox
Terminal=false
Icon=/opt/firefox_dev/browser/icons/mozicon128.png
Type=Application
Categories=Application;Network;X-Developer;
Comment=Firefox Developer Edition Web Browser.
```

Now open you file explorer, search for `/opt` folder, open it, open `firefox_dev` folder and open `firefox`

Tip: *Pin this to the taskbar for easy startup*

Also install the security update ppa using:

```
sudo add-apt-repository ppa:ubuntu-mozilla-security/ppa
sudo apt-get update
```

## Install .deb files

Refer: http://unix.stackexchange.com/questions/159094/how-to-install-a-deb-file-by-dpkg-i-or-by-apt

## To install sublime text 3

Install the GPG key:
```
wget -qO - https://download.sublimetext.com/sublimehq-pub.gpg | sudo apt-key add -
```
Ensure apt is set up to work with https sources:
```
sudo apt-get install apt-transport-https
```

Select the channel to use:
Stable
```
echo "deb https://download.sublimetext.com/ apt/stable/" | sudo tee /etc/apt/sources.list.d/sublime-text.list
```

Dev
```
echo "deb https://download.sublimetext.com/ apt/dev/" | sudo tee /etc/apt/sources.list.d/sublime-text.list
```

Update apt sources and install Sublime Text
```
sudo apt-get update
sudo apt-get install sublime-text
```

Tips: *To set it as the default text editor*
Refer: http://askubuntu.com/questions/396938/how-do-i-make-sublime-text-3-the-default-text-editor

## Remove unwanted architectures
*Note: DO NOT follow this unless you're using a 64-bit CPU*
```
dpkg --print-architecture
```
should print `amd64`
```
dpkg --print-foreign-architectures
```
should print `i386` only
```
sudo dpkg --remove-architecture foreign-architecture
```
`foreign-architecture` since it was unwanted

## To install wineHQ on Ubuntu

Wine is used to emulate any Windows Programs which you desperately need in Linux.
TL;DR Refer: http://askubuntu.com/questions/316025/how-to-install-and-configure-wine

```
sudo dpkg \--add-architecture i386
sudo add-apt-repository ppa:wine/wine-builds
sudo apt update
sudo apt install winehq-staging
winecfg
```

## To install uTorrent

Refer: https://www.youtube.com/watch?v=oSiUcgGyiGM

<http://localhost:8080/gui>

User:admin
Password:

(Yes the password is blank)

## To install IDM

Refer: http://askubuntu.com/questions/554062/how-i-can-install-internet-download-manager-on-ubuntu-14-04

## To install VLC
```
sudo apt update
sudo apt install vlc browser-plugin-vlc
```

Refer: http://www.videolan.org/vlc/download-ubuntu.html

*Note: To fix the overlay problem, goto **Tools** > **Preferences** > **Video**, and uncheck **Accelerated Video Output (Overlay)***

## Why use apt update

Ref: http://askubuntu.com/questions/337198/is-sudo-apt-get-update-mandatory-before-every-package-installation

## To be able to recognize exfat formatted USB drives:

```
sudo apt-get install exfat-fuse exfat-utils
```

## To install pip

For pip you'll need python as well on your system. Python 2 is the default on Ubuntu, however I suggest you migrate to 3
```
sudo apt install python3
sudo curl https://bootstrap.pypa.io/3.2/get-pip.py | python3.6
```

## In case of compatibility issues (causing IGN in terminal)

Ref: https://discuss.elastic.co/t/apt-repositories-are-failing-with-404/47713/7

## Setting up virtualenv

Refs: TL;DR
http://stackoverflow.com/questions/5506110/is-it-possible-to-install-another-version-of-python-to-virtualenv

To install virtual environment
```
sudo pip install virtualenv
```

To create a virtual environment
```
mkdir Environments
cd Env*
virtualenv project1_env
```

To enter the virtual environment
```
source project1_env/bin/activate
```

To exit the virtual environment
```
deactivate
```

*Note: This virtual environment is only for the python packages you install. You need not use `sudo` to install packages using `pip`*

## To install Git

```
apt install git
```

## To install Android Studio

Ref: https://developer.android.com/studio/index.html

## To install Zotero

Ref: https://www.zotero.org/download/

## To quit anything abruptly in terminal

`Ctrl + C`

## To close terminal

```
exit
```

## Difference between "cd ~/folder_name" and "cd folder_name"

The tilda (i.e. \~) before the folder path indicates that it is located in home directory, and without tilda it is located in the current directory.

## To install opencv

Steps for installing opencv 2 in python 2
Ref:
1. http://docs.opencv.org/2.4/doc/tutorials/introduction/linux_install/linux_install.html#linux-installation
2. https://medium.com/@manuganji/installation-of-opencv-numpy-scipy-inside-a-virtualenv-bf4d82220313#.ndrkgkel7

Remember:

These are the parameters I passed to the `cmake` command for opencv 2 on python 2
```
cmake -D MAKE_BUILD_TYPE=RELEASE -D CMAKE_INSTALL_PREFIX=$VIRTUAL_ENV/local/ -D PYTHON_EXECUTABLE=$VIRTUAL_ENV/bin/python -D PYTHON_PACKAGES_PATH=$VIRTUAL_ENV/lib/python2.7/site-packages -D WITH_TBB=ON -D BUILD_NEW_PYTHON_SUPPORT=ON -D WITH_V4L=ON -D INSTALL_C_EXAMPLES=ON -D INSTALL_PYTHON_EXAMPLES=ON -D BUILD_EXAMPLES=ON -D WITH_QT=ON -D WITH_GTK=ON -D WITH_OPENGL=ON ..
```

These are the parameters I passed to the `cmake` command for opencv 3 in python 3
```
cmake -D CMAKE_BUILD_TYPE=RELEASE -D CMAKE_INSTALL_PREFIX=$VIRTUAL_ENV/local/ -D INSTALL_C_EXAMPLES=OFF -D INSTALL_PYTHON_EXAMPLES=ON -D PYTHON_EXECUTABLE=$VIRTUAL_ENV/bin/python -D PYTHON_PACKAGES_PATH=$VIRTUAL_ENV/lib/python3.5/site-packages ..
```

```
sudo apt-get install liblapacke-dev checkinstall
make -j4
sudo checkinstall
```

Ref:
1. http://www.pyimagesearch.com/2015/07/20/install-opencv-3-0-and-python-3-4-on-ubuntu/
2. http://answers.opencv.org/question/121651/fata-error-lapacke_h_path-notfound-when-building-opencv-32/
3. http://docs.opencv.org/3.0-beta/doc/tutorials/introduction/linux_install/linux_install.html

*Note: Remember opencv2 cannot be installed with python 3*

## To install other useful packages

```
pip install scipy
pip install matplotlib
pip install pandas
pip install dlxsudoku
pip install -U scikit-learn
pip install ipython
pip install graphviz
pip install pydotplus
sudo apt install python-xlib python-tk
pip install Image
pip install python-xlib
pip install PyAutoGUI
```

Refs:
1. http://www.numpy.org/
2. https://www.scipy.org/
3. http://matplotlib.org/
4. http://pandas.pydata.org/
5. https://pypi.python.org/pypi/dlxsudoku
6. http://scikit-learn.org/stable/
7. https://ipython.org/
8. http://www.graphviz.org/
9. https://pypi.python.org/pypi/pydotplus
10. http://jupyter.org/
11. https://pyautogui.readthedocs.org/

## To install tensorflow

Ref: https://www.tensorflow.org

## To execute python files using terminal
```
python filename.py
```

## To cd to a directory whose name has a space in it

```
cd folder\ name\ with\ spaces\ in\ it
```
The backslash acts as an escape character.

## To use IP camera as webcam in Python

Ref: https://junise.wordpress.com/2015/05/01/stream-from-wifi-ip-camera-android-phone-wifi-ip-camera-using-python/

## To setup Sublime Text 3

1. Open Sublime Text.
2. Go to 'View', and then to 'Show Console'.
3. Visit this site: https://packagecontrol.io/installation#st3 and copy the code snippet under the heading "SUBLIME TEXT 3".
4. Paste it in the console of Sublime Text and press 'Enter'. Package Control will be installed.
5. In Sublime press 'Ctrl + Shift + P'
6. Type in "install"
7. Click on "Package Control: Install Package".
8. Then select SublimeREPL. It will install it automatically.
9. Again in Sublime press 'Ctrl + Shift + P', type in "install". Again click on "Package Control: Install Package". Then select "SublimeLinter". This will install it automatically.
10. Again in Sublime press 'Ctrl + Shift + P', type in "install". Again click on "Package Control: Install Package". Then select "LaTeXing". This will install it automatically.

## To shutdown PC from terminal
```
sudo poweroff
```

## To reboot PC from terminal
```
sudo reboot
```

## To install arduino

Ref: https://www.arduino.cc/en/Guide/Linux

*Note: Before uploading to the board you'll always have to execute these commands in the terminal. (put the respective port it's connected to instead of 'ttyACM0' below)

```
sudo usermod -a -G dialout username
sudo chmod a+rw /dev/ttyACM0
```

## To run an ftp server

Refs:
1. https://help.ubuntu.com/lts/serverguide/ftp-server.html
2. https://www.youtube.com/watch?v=wXSuqzwLnL4yu
3. http://askubuntu.com/questions/649935/installed-15-04-cannot-restart-ssh-daemon
4. http://askubuntu.com/questions/198567/vsftpd-installed-but-i-cant-restart-stop-it

The username and password will be your ubuntu username and password

Notes:
vsftpd is on by default, to uninstall:
```
sudo apt remove vsftpd
```

## To check network activity

Ref: http://askubuntu.com/questions/37847/is-there-a-command-that-returns-network-utilization

## To check the manual for any command
```
man command
```

## To remove or uninstall any unwanted packages
```
sudo apt-get remove package
```

## To monitor network activity

Ref: http://askubuntu.com/questions/532424/how-to-monitor-bandwidth-usage
```
sudo nethogs wlo1
```

## To install steam

Ref: https://linuxconfig.org/how-to-install-steam-on-ubuntu-16-04-xenial-xerus

## To transfer data between two PCs

Ref: http://askubuntu.com/questions/475697/how-can-i-transfer-data-between-two-computers-using-ethernet-cable-and-ftp-softw

## To run C programs
To compile the file:
```
gcc file_name.c -o compiled_file_name
```
To execute the compiled binary:
```
./compiled_file_name
```

## To run C++ programs**
To compile the file:
```
g++ file_name.cpp -o compiled_file_name
```
To execute the compiled binary:
```
./compiled_file_name
```

## To install django
```
pip install django
```
Ref: https://docs.djangoproject.com/en/1.10/

## To install MEGAsync
```
sudo dpkg -i megasync-xUbuntu_16.04_amd64.deb
sudo apt-get -f install
```

## To resize partitions

Ref: http://askubuntu.com/questions/291888/can-i-adjust-reduce-my-partition-size-for-ubuntu

## To remote control Ubuntu

Ref: http://askubuntu.com/questions/155477/how-do-i-remotely-control-another-ubuntu-desktop-from-ubuntu

## To install docker

Ref: https://docs.docker.com/engine/installation/linux/ubuntulinux/

## To install ROS Kinetic Kane and Turtlebot

It is highly recommended to install ROS on docker. This is to prevent
dependency issues. Some known issues are:

1.  ROS Kinetic isn't compatible with turtlebot. However it's working as of now. No bugs encountered yet.
2.  ROS Indigo is compatible with turtlebot, however Ubuntu 16.04 isn't compatible with ROS Indigo.

Refs:
1. http://wiki.ros.org/kinetic/Installation/Ubuntu
2. http://wiki.ros.org/turtlebot/Tutorials/indigo/Turtlebot%20Installation

The 2nd link is for installing turtlebot on ROS. Since we're using ROS Kinetic replace "indigo" in all package names in the turtlebot installation with "kinetic". Two packages won't be available. Delete them, don't install them.
```
roslaunch turtlebot_gazebo turtlebot_world.launch
roslaunch turtlebot_teleop keyboard_teleop.launch
```

*To uninstall ROS Kinetic Kane and Turtlebot*

```
sudo apt-get purge ros-*
sudo apt-get autoremove
```

Then modify the `~/.bashrc` file by deleting the line `source
opt/ros...`

## To install sox
```
sudo apt-get install sox
```
Ref: http://sox.sourceforge.net/

## To install theano

Ref: http://deeplearning.net/software/theano/install.html

```
pip install nose
pip install nose-parameterized
```

## To increase swap size in Ubuntu

Ref: http://askubuntu.com/questions/178712/how-to-increase-swap-space

## To open jupyter notebook in a particular directory

```
jupyter notebook /path/to/directory
```

## To install Pytables
```
pip install tables
```

Ref: http://www.pytables.org/usersguide/installation.html

## To install SublimeHighlight in Sublime Text

Ref: http://stackoverflow.com/questions/21037711/sublime-text-2-paste-with-colors-to-ms-word

*Note: The name of the package is "Highlight" and not "SublimeHighlight"*

Set the theme as "monokai"

## To install Compare Side-By-Side in Sublime Text

Ref: https://packagecontrol.io/packages/Compare%20Side-By-Side

*Note: Could consider "Sublimemege 3" also

## To install other useful packages
```
pip install h5py
```
Ref: http://docs.h5py.org/en/latest/index.html

## How to connect Tata photon +(Huawei EC156) dongle

Ref: http://askubuntu.com/questions/536371/how-to-connect-tata-photon-huawei-ec156-in-ubuntu-14-04

## Uninstall package built from source

Ref: http://unix.stackexchange.com/questions/64759/how-do-i-remove-uninstall-a-program-that-i-have-complied-from-source

Problems were faced while uninstalling octave-4.0.3 built from source, and after following the above steps also had to remove the files found using bash individually using sudo rm command. To avoid such situations in the future, consider using checkinstall instead of make install.

Ref: https://help.ubuntu.com/community/CheckInstall

## To install Octave

Ref: http://wiki.octave.org/Octave_for_Debian_systems#Compiling_from_source

Use `./configure --with-hdf5-includedir=/usr/include/hdf5/serial --with-hdf5-libdir=/usr/lib/x86_64-linux-gnu/hdf5/serial JAVA_HOME=/usr/lib/jvm/java-7-openjdk-amd64`

*Note: After `make`, instead of `sudo make install` do `sudo checkinstall`*

## Using LaTeX in IPython notebooks (Jupyter Notebook)

Just put your LaTeX math inside `$$`

## How to create WiFi hotspot using Ubuntu

Goto "Edit connections", "Add", select "WiFi". Choose an "SSID" (name) for the network. Select "hotspot". Select the "2.4 GHz" band. And click on "Save"

## How to execute MATLAB/Octave code in terminal

Type 'octave file_name.m'

You cannot call octave functions using the above method. You'll have to use octave for the same or call the function name inside an `m` file and then execute it using octave.

## To install Octave kernel in IPython notebooks (Jupyter Notebook)

```
pip install octave_kernel
python -m octave\_kernel.install
```
Ref: https://github.com/Calysto/octave_kernel

## To install Octave-Forge package in Octave
```
pkg install -forge package\_name
```
Ref: http://octave.sourceforge.net

## To uninstall Octave-Forge package in Octave
```
pkg uninstall package_name
```

It is not recommended to install the unmaintained Octave-Forge packages like nnet, due to compatibility issues. Instead you could download the `m` files for the required function and modify it accordingly.

## Create a function in Octave which is available in MATLAB but not in Octave

Create an `m` file the "file_name" same as the "function_name" contained within. Understand the function definition from the MATLAB help page online. Accordingly modify the function contents and save it in the same directory where it is called.

## How to remove an added ppa

Ref: http://askubuntu.com/questions/307/how-can-ppas-be-removed

## To install XDM (Xtreme Download Manager)

Download the tar.gz file, unzip it. cd into the unzipped folder. Execute
the command `sudo ./install.sh`

Ref: http://xdman.sourceforge.net/#downloads

## To install unity tweak tool
```
sudo apt install unity-tweak-tool
```

## To install various themes and icons for Ubuntu

For Macbuntu theme:
```
sudo add-apt-repository ppa:noobslab/macbuntu
sudo apt-get update
sudo apt-get install macbuntu-os-icons-lts-v7
sudo apt-get install macbuntu-os-ithemes-lts-v7
```

For Arc theme and icons:
```
sudo add-apt-repository ppa:noobslab/themes
sudo apt-get update
sudo apt-get install arc-theme
sudo add-apt-repository ppa:noobslab/icons
sudo apt-get update
sudo apt-get install arc-icons
```

For Vivacious theme and icons:
```
sudo add-apt-repository ppa:ravefinity-project/ppa
sudo apt-get update
sudo apt-get install vivacious-colors-gtk-dark
```

## To install network speed indicator
```
sudo add-apt-repository ppa:nilarimogard/webupd8
sudo apt-get update
sudo apt-get install indicator-netspeed
```

## To install system monitor indicator
```
sudo apt-get install indicator-multiload
```

## To uninstall ImageMagic
Don't remove it. It might harm your system.

Ref: http://askubuntu.com/questions/764553/how-to-uninstall-image-magick

## XMind

To install:
Ref: http://www.xmind.net/download/linux/>

To uninstall:
Open "Ubuntu Software" goto the "Installed" tab and "Remove" XMind

## Install new themes in jupyter

One way to do this is to replace the `custom.css` file in `~/.jupyter/custom`

Or you can also tweak using the command line.

Ref: https://github.com/dunovank/jupyter-themes

## To install texlive
```
sudo apt-get install texstudio
sudo apt-get install texlive-full
```
Refs:
1. http://www.texstudio.org/
2. https://www.tug.org/texlive/

## Using namebench to determine best DNS server for you

Ref: https://code.google.com/archive/p/namebench

## To change DNS

Goto "Edit connections" by clicking on the WiFi symbol in the notification bar. Select your WiFi connection and click on edit. Goto IPv4 settings and change method to "DHCP automatic (addresses only)". Now in the DNS field write the address and save it. Now restart the WiFi connection by clicking on your network again via the notification bar.

Ref: https://developers.google.com/speed/public-dns/docs/using

## To install Kodi
```
sudo apt-get install software-properties-common
sudo add-apt-repository ppa:team-xbmc/ppa
sudo apt-get update
sudo apt-get install kodi
```

Ref: http://kodi.wiki/view/HOW-TO:Install_Kodi_for_Linux

## To install Go
```
sudo apt-get install golang
```

## To install NodeJs
```
curl -sL https://deb.nodesource.com/setup_10.x | sudo -E bash -
sudo apt-get install -y nodejs
sudo apt-get install -y build-essential
```

Ref: https://nodejs.org/en/download/package-manager/

## LIFE SAVER for graphics issues caused by CUDA

Ref: https://askubuntu.com/questions/760934/graphics-issues-after-while-installing-ubuntu-16-04-16-10-with-nvidia-graphics

## CUDA installation references

Ref:
1. https://askubuntu.com/questions/57994/root-drive-is-running-out-of-disk-space-how-can-i-free-up-space
2. http://docs.nvidia.com/cuda/cuda-installation-guide-linux/index.html#post-installation-actions
3. https://askubuntu.com/questions/799184/how-can-i-install-cuda-on-ubuntu-16-04
4. https://askubuntu.com/questions/767269/how-can-i-install-cudnn-on-ubuntu-16-04

**Note: Don't do it in a virtualenv**

Export path needs to be added to `~/.bashrc`
```
export PATH=/usr/local/cuda/bin${PATH:+:${PATH}}
export LD_LIBRARY_PATH=/usr/local/cuda/lib64${LD_LIBRARY_PATH:+:${LD_LIBRARY_PATH}}
export CUDA_HOME=/usr/local/cuda${CUDA_HOME:+:${CUDA_HOME}}
```

## Symbolic links are like shortcuts in windows

## Caffe installation references

Ref: https://software.intel.com/en-us/forums/intel-math-kernel-library/topic/509013

Install order should be:

*Virtualenv, numpy, scipy etc, openblas, OpenGL---present on system already, intel mkl, nvidia drivers, numpy scipy, cuda, cudnn, opencv, caffe, tensorflow, tflearn, theano, openai, keras, pytorch*
