---
title: "Firewall init"
publishdate: 2018-02-11T12:34:41-05:00
tags: ["firewall","ufw","linux","hardening"]
type: "post"
draft: false
showDate: true
comments: false
---


Firewall is used to keep a check on the incoming and outgoing connections.

We shall be using `ufw` (**Uncomplicated Firewall**) to close unwanted incoming connections from the Internet and allow outgoing ones.

It's preferable to not use Scaleway servers, as something or the other used to get messed up on those. Digital Ocean and Vultr seem to do just fine.

Make sure this is the first thing you do when setting up a server. This is to ensure, there is no data loss or time loss if anything goes wrong. There is a major chance of losing complete access to the server in case you don't configure something properly. So make sure to backup your data locally or on another server to prevent any data loss.

- Install `ufw`

```sh
apt install ufw
```


- Edit `/etc/default/ufw` and modify all the three lines as shown below

```
DEFAULT_INPUT_POLICY="ACCEPT"
DEFAULT_INPUT_POLICY="ACCEPT"
DEFAULT_INPUT_POLICY="ACCEPT"
```

- Append a drop-all rule to the INPUT chain: Edit `/etc/ufw/after.rules`, add this line just before the final `COMMIT` line:

```
-A ufw-reject-input -j DROP
```

- Disable `ufw` logging (this seems to cause issues with Scaleway's default kernel):

```sh
ufw logging off
```

That's it, `ufw` is up and running, and NBD shouldn't cause issues.

- This is also necessary since there seems to be some permissions issue with the following folders

```sh
chmod 751 /etc/default
chmod 751 /etc
chmod 751 /usr
```

- Setup a basic configuration to allow SSH, HTTPS and HTTP incoming

```sh
ufw default deny incoming
ufw default allow outgoing
ufw allow ssh
ufw allow 80/tcp
ufw allow 443/tcp
ufw enable
```

In a new terminal window check that you can still access your host via ssh.

- You can check the configuration at any time with:

```sh
ufw status verbose
```

- You can disable the `ufw` configuration at any time with:

```sh
sudo ufw disable
```

