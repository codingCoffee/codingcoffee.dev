---
title: "The Resolvable Paranoia"
publishdate: 2018-08-19T13:56:12-05:00
tags: ["ssh","gpg", "keys", "security", "signing"]
type: "post"
draft: false
showDate: true
comments: false
---

Ahh, where should we start? Okay, let's start with, I'm the relatively paranoid type.

And given the super ambitious nature of agencies like NSA and governments around the world, I didn't want to leave any blind spots. So, I decided, that it's about time we do this!

Throughout this blog I'm going to be using mostly layman terms, while citing the technical sources, which you can checkout if you want to.

Also this blog is not targetted at you if you are an ameture, because if you are one, you are probably already doing 10 things incorrectly, and this is the least of your worries. You can still have a good read though. The blog mostly targets intermediate developers and intends to be the single place you need to look for setting up a secure machine.


## SSH and GPG keys

Alright so, being on a linux platform, there are going to be 2 types of Keys you ever need to be worried about.


### SSH Keys

You'll be using these keys as a sort of authentication mechanism, to prove your identity. Popular examples of using these keys is when you need passwordless access to servers or maybe to push commits to your git repository. It is not technically passwordless, since your Key act as your password. They are composed of 2 parts, the public key, and the private key. The public key, sits on the server which you want to access, you can openly disclose it to the world without fearing any compromises, whereas the private key is supposed to stay with you.


### GPG Keys

Next are the GPG Keys, now these are a little more complicated than SSH Keys. GPG Keys can be used for signing messages, or commits, or software, to provide authenticity, of your identity, to prove that you are indeed the one providing the information. They can also be used to encrypt messages, so that they can be read by only the person or organization it is intended for.


## How many keys does a man require?

Seems like the modern version of Leo Tolstoy's story. Anyways jokes aside. With more number of keys comes more security, but also more complexity in handling the keys. To start off lets lay down some logical statements.

1. Keys shouldn't be transfered between devices. Why you ask, well, if you have the same set of keys everywhere, and if one of your devices is compromised then, your entire identity is compromised. However if you have separate keys, you may easily block access to the key present on your compromised device.
2. Keys shouldn't be passwordless. This ensures even in case of a hardware compromise, the keys are unusable. For instance let's say the malicious actor gets hold of your devices, and has his files his hands on the keys, he is still unable to use them without the password which only you know.
3. It's a bad idea to use the same keys for a lifetime. This is a sort of precausionary measure. Assuming the malicious actor alrady has your keys, but hasn't used them for neferous purposes, the threat will be neutralized upon key renewal. Also for renewal it is a good idea to maintain a list of places where the public key has been uploaded, for example GitHub, GitLab, personal server etc., so that it can easily be replaced. I replace them every year. Maybe as I get more paranoid, I'll decrease the duration :)
4. Not to use the the same GPG Key for signing and encrypting. So explaining this one is a little complicated, and will require a bit of math. You may look it up [here](https://crypto.stackexchange.com/questions/12090/using-the-same-rsa-keypair-to-sign-and-encrypt), but like other logical assumptions I've made above, this one also stands.
5. The GPG keys which you use for signing, shouldn't be replaced. These are the only keys, you need to protect with a great deal of precaution. The reason for this is, it will be used to build your web of trust. Replacing this unverifies all your signed entities. Now it is not an end of the world scenatio as I'm making it out to be. I mean nothing will stop working or anything, it's just that, the way I work, I try to seek perfection where possible. And this might be a little far fetched, but I think it is achievable. So in an ideal world, if I sign a commit, and it is verified, I intend it to be verified till the end of time. If my signing keys are compromised, I'll have to remove them, and have them replaced, rendering all my older commits unverfied. It has happened before and I'm not proud of it.
6. Now you may say, that maintaining so many GPG Keys, and that too securely, will be a painful task. But don't worry, OpenPGP has got you covered. It has this concept of [subkeys](https://wiki.debian.org/Subkeys), wherein the main key, need not even be on any of the devices you're using. Which brings me to my next point, to not have the main GPG Key on any of your devices, and rather keep it on an air-gapped device, for super security.
7. Do not delete old keys using `rm`. Use `shred` instead.
8. Use FLOSS only. Okay maybe I'm going a bit overboard with this one. But if you truly want to use secure systems, you need to be able to trust the softwares which you're using. And the best way to do this is to use softwares which have their code out in the open for anyone to review. Don't get me wrong, I'm not saying FLOSS softwares don't have any security flaws in them. All I'm trying to say is, given the codebase is out in the open, a lot of people have eyes on the code to spot the flaws, have them reported and get them fixed. I also get it, that sometimes it is a little hard to do this. Maybe you desperately want to play that game, or want to use that one software which gets everything right for you. One way I could think of resolving this problem is to have a dual boot system, one with FLOSS. I for the most part have replaced most of my applications with FLOSS.
9. For SSH keys, prefer eliptical curve cryptography(ECC) over RSA. ECC keys are a lot shorter, while providing the same level of security as RSA. There are a few more benefits to using ECC, since they are immune to side channel attacks. To generate an RSA you have to generate two large random primes, and the code that does this is complicated an so can more easily be (and in the past has been) compromised to generate weak keys. However certain older systems might not support ECC and hence it is a good idea to have an RSA Key as backup incase ED25519 is not implemented.


## Ground Rules

Based on the rules above, I think it is same to assume the following ground rules

1. Every system should have it's own key.
2. Keys should have a passphrase on them.
3. Replace keys every year.
4. The GPG keys used for signing should'nt be replaced.
5. Use different GPG Keys for signing and for encrypting.
6. Keep your main GPG Key on an air-gapped device.
7. Delete keys using `shred` instead of `rm`
8. Use FLOSS preferably.
9. For SSH, have 2 keys one using ED25519 implementation, and the other RSA.

Boy we've reached 9 rules. I was expecting 3 or 4 at max. Anyways, we gotta get rid of the paranoia.


## Storge Mechanisms and Backups

The next problem arises of storing the keys, and having backups. You need to have backups, because it is a bad idea to put all your e̶g̶g̶s̶ keys in one b̵a̵s̵k̵t̵e̵t̵ device. Your PC may fatally crash, your phone may go dead, or you USB stick may just stop responding. Hence it is a bad idea to have everything at a single location.

Here we follow the 3-2-1 backup rule. It is a best practice to have 3 copies of the data. This way, even if you lose any 2 of them, which in itself is an unlikely occurence, you can still recover.

You may also use something like the physical prinout of you key on paper, using something like [paperkey](http://www.jabberwocky.com/software/paperkey). Or maybe in the form of a QR code, maybe at the bottom of the ocean/ in someone's grave/ inside a nuclear reactor. Whatever, get creative with your ideas! Or maybe keep it simple, and have multiple plain old USB sticks in differnt locations, or a CD-ROM (if anyone even uses that nowadays). Or you could store it on Google Drive, defeating the entire purpose and effort. The cloud could also be considered an option, but then again, I won't necessarily trust the cloud provider to not access my data.

Now earlier I had suggested that it is a bad idea to store it on your device, However you may also store, inside a [tomb](https://github.com/dyne/Tomb).  More on that later.


## Random number geenration

Before key creation you may want to install `rng-tools` for entropy creation.

## Creating a SSH Key

Alright now coming to the creation part. Finally comes the creation of the key part! The initial seed should be super random.
Make sure you have OpenSSH installed on your system. Most systems have it, however in case it is not present, simply look it up on the internet, and install it using your package manager.

Creating the ED25519 Key:
```sh
ssh-keygen -a 100 -t ed25519 -f ~/.ssh/id_ed25519 -C "john.doe@mail.com"
```

Creating the RSA Key:
```sh
ssh-keygen -a 100 -b 4096 -f ~/.ssh/id_rsa -C "john.doe@mail.com"
```

In both the cases you'll have to enter your passphrase, while creating the key. Make sure you choose a secure passphrase, preferably 10 characters long, with a combination of symbols, number, capital and small lettes. Also make sure you don't forget it!


## Creating a GPG Key

Make sure you're using `gpg2` instead of `gpg`.

The creation process for the 2 GPG keys is similar, except for some minor changes. To start the process you need to invoke the same command in both the cases.

```sh
gpg2 --verbose --full-gen-key
```

Let's create the signing key first.

You'll be presented with a few prompts. For the 1st prompt, select 4, i.e. `RSA (sign only)`. Next make sure your keys are `4096` bits long. As explained above, given this is a the signing key, the validity of the key should be `0`, i.e. the key does not expire. Finally you'll need to confirm your inputs, enter your passphrase insert you name, email ID, and a comment. In the comment, you may mention `Signing Key`. Note that, the last 3 fields, are inconssequential to the key generation, however they're important for identification purposes. Done, your signing key will be created.

For creation of the encryption key, invoke the same command as above.

Filling the prompts will be a little different this time around. For the 1st prompt, select 1, i.e. `RSA and RSA`. Next make sure your keys are `4096` bits long. For key expiration period, you may select 1 year, i.e. `1y`. Confirm everything and enter a passphrase like above, and your encryption keys are also ready!

Note again, I cannot emphasise enough the importance of using a strong passphrase! The passphase will be what protects your private key in case it is stolen.

