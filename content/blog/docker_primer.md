---
title: "Docker Primer"
publishdate: 2018-02-13T13:56:12-05:00
tags: ["docker","containers"]
type: "post"
draft: false
showDate: true
comments: false
draft: false
---

When we think of virtualization today, we may think of Virtual Box, which abstracts away the system processes, and lets you run a completely system from another. Think of Docker as Virtual Box, but extremely lightweight (in terms of resource consumption). Obviously I'm over simplifying the explanation a little, and a whole of things are getting lost in simplification. But for now, this will do.

## Installation

-  Install docker

```shell
apt remove docker docker-engine docker.io
apt update && apt -y upgrade
apt install -y linux-image-extra-$(uname -r) linux-image-extra-virtual #FOR UBUNTU 18: apt install -y linux-image-$(uname -r) linux-image-extra-virtual
apt update && apt -y upgrade
apt install -y apt-transport-https ca-certificates curl software-properties-common
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | apt-key add -
add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable" #FOR UBUNTU 18: instead of stable use edge (just for now, since they haven't released the stable version on 18.04 yet)
apt update && apt -y upgrade
apt install -y docker-ce
```

-  Install docker-compose

```shell
curl -L https://github.com/docker/compose/releases/download/1.21.2/docker-compose-$(uname -s)-$(uname -m) -o /usr/local/bin/docker-compose #use the latest docker-compose version, by looking at releases on github pages [https://github.com/docker/compose/releases]https://gist.githubusercontent.com/mamigot/1abdefc684f97d3476f177d85248ff36/raw/5f57b4918b237f8e0127b45984fa51a483218349/docker-compose.yml
chmod +x /usr/local/bin/docker-compose
```

## Post Installation stuff

Allow docker usage without sudo access

The docker daemon binds to a Unix socket instead of a TCP port. By
default that Unix socket is owned by the user root and other users can
only access it using sudo. The docker daemon always runs as the root
user. To prevent using sudo when you use the docker command, create a
Unix group called docker and add users to it. When the docker daemon
starts, it makes the ownership of the Unix socket read/writable by the
docker group

```shell
sudo groupadd docker
sudo usermod -aG docker $USER
newgrp docker
```

## Terminology

-  Images

Images are read only We upload images to hub. Think of them as classes in an object oriented programming language terminology.

-  Containers

Containers boot up from images. Think of them as instances of a class.

- Tags

You can think of this as versions

- Container ID or Image ID

Unique ID assigned to every container or image respectively

## Usage

- A `Dockerfile` can be used to build docker images

- To list all images available on your system

```shell
docker images
```

- To list all containers

```shell
docker ps -a
```

- To pull images from the docker hub, for example for pulling the
    ubuntu image

```shell
docker pull ubuntu
```

By default the `latest` `tag` is pulled. Consider `tag` to be sort of like the version of the image you want.

- To pull a specific tag, lets say the `tag` is `12.04`

```shell
docker pull ubuntu:12.04
```

- To delete an image

```shell
docker rmi ubuntu:latest
```

or

```shell
docker rmi IMAGE_ID
```

- To create a `container` from an `image` and run it

```shell
docker run -it ID /bin/bash
```

- `i` stands for interactive
- `t` stands for pseudo tty. It is for specifying the path to the shell to be run.
- `ID` can be an `IMAGE-ID` or a `CONTAINER-ID` obtained by listing all images, or containers respectively.

Note: Every time you use `docker run` using the IMAGE-ID, a new
container is created based on the `image`

Extra bits: If the argument `--rm` is if passed, causes the container to automatically get deleted after exiting the shell.

- To exit a container

```shell
exit
```

- To start a container

```shell
docker start CONTAINER_ID
```

- To stop a container

```shell
docker stop CONTAINER_ID
```

- To delete a container

```shell
docker rm CONTAINER_ID
```

- To build your own docker image using a `Dockerfile`. Create a `Dockerfile` and put it in a folder, and execute the following command

```shell
docker build -t username/image_name:image_tag .
```

`username` - as on hub.docker.com
`image_name` - the name of the image you want to build
`image_tag` - the tag you want to assign to the image

- To delete dangling images (unused images present on your system)

```shell
docker rmi $(docker images -f dangling=true -q)
```

- Save an existing docker container as an image

```shell
docker commit CONTAINER_ID IMAGE_NAME:IMAGE_TAG
```

-  Saving an image as a tar file

```shell
docker save IMAGE_ID -o file_name.tar
```

