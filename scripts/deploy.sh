#!/bin/bash

# Check helm chart is installed or create
# reuse installed values and resets data

export CHECK_AS=$(helm ls -q cc-blog --tiller-namespace cc)
if [ "$CHECK_AS" = "cc-blog" ]
then
    echo "Updating existing cc-blog . . ."
    helm upgrade cc-blog \
        --tiller-namespace cc \
        --namespace cc \
        --reuse-values \
        --recreate-pods \
        --wait \
        helm-chart/cc-blog
fi

