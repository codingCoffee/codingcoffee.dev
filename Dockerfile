FROM alpine as builder


# Install packages
RUN set -ex \
  && apk add --no-cache \
    hugo \
    git \
  && mkdir -p /var/www/blog

COPY . /var/www/blog

WORKDIR /var/www/blog
RUN set -ex \
  && git submodule update --init --recursive \
  && hugo

# Distributable image layer
FROM nginx:alpine
COPY --from=builder /var/www/blog/public/ /usr/share/nginx/html

