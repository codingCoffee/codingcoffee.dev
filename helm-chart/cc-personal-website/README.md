
# Install

```sh
helm install --name cc-personal-website --tiller-namespace cc cc-personal-website
```

# Uninstall

```sh
helm delete --purge cc-personal-website
```

