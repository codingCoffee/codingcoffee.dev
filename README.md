
## Personal Website

Source for https://codingcoffee.dev
``

## Notes

- The homepage, i.e. `index.html` is from the
  [hugo-coder](https://github.com/luizdepra/hugo-coder) theme. While making
  modifications it needs to be manually edited.

